import { overnightAJSPage } from './app.po';

describe('day-care-ajs App', function() {
  let page: overnightAJSPage;

  beforeEach(() => {
    page = new overnightAJSPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
