using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using overnight.Db;
using overnight.Models;
using overnight.Models.Security;

namespace  overnight.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class HomeController : BaseController 
    {
        public HomeController():base()
        {
        }

        public IActionResult Index() 
        {
            return View();
        }
    }
}