﻿using System;
using System.Collections.Generic;

namespace overnight.Models
{
    public class Tag : BaseEntity<Int64>
    {
        public List<PostTag> Posts { get; set; }
    }
}
