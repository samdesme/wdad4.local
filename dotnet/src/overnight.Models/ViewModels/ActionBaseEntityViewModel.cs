using System;
using overnight.Models;

namespace overnight.Models.ViewModels
{
    public class ActionBaseEntityViewModel<T>: ActionViewModel
    {
        public BaseEntity<T> BaseEntity { get; set; }
    }
}