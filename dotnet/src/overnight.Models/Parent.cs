using System;
using System.Collections.Generic;

namespace overnight.Models
{
    public class Parent : Person
    {
        public List<ParentChild> Children { get; set; }
    }
}
