﻿using System;
using System.Collections.Generic;

namespace overnight.Models
{
    public class Organisation : BaseEntity<Int16>
    {
        public List<Location> Locations { get; set; }
    }
}
