using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using overnight.Models;
using overnight.Models.Security;

namespace overnight.Db
{
    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseNpgsql("User ID=postgres;Password=W-TG34P3;Host=localhost;Port=5432;Database=overnight;Pooling=true;");
            return new ApplicationDbContext(builder.Options);
        }
    }
}